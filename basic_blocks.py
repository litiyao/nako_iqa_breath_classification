# model test
import os
#os.environ["CUDA_DEVICE_ORDER"]="0000:02:00.0"


from tensorflow.python.client import device_lib
print(device_lib.list_local_devices)

import tensorflow as tf
import os.path
import scipy.io as sio
import numpy as np
import math
import keras
from keras.layers import Input
import keras.backend as K
from keras.layers import Conv2D, Conv3D
from keras.layers import BatchNormalization
from keras.layers import GlobalAveragePooling2D
from keras.activations import softmax
from keras.layers import concatenate
from keras.layers.core import Dense, Activation, Flatten
from keras.models import Model
from keras.models import Sequential
from keras.layers import UpSampling3D
from keras.layers.convolutional import Convolution2D
from keras.layers import LeakyReLU
from keras.layers import Softmax
from keras.callbacks import EarlyStopping
from keras.callbacks import LearningRateScheduler
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import ModelCheckpoint
from keras.models import model_from_json
from keras.regularizers import l2  # , activity_l2

from keras.optimizers import SGD
from deep_residual_learning_blocks import * #
from matplotlib import pyplot as plt

import scipy.io as sio


def createModel(patchSize, numClasses, usingClassification=False):

    if K.image_data_format() == 'channels_last':
        bn_axis = -1
    else:
        bn_axis = 1

    input_tensor = Input(shape=(patchSize[0], patchSize[1], patchSize[2], 2))

    # first stage
    x = Conv3D(filters=16,
               kernel_size=(5, 5, 5),
               strides=(1, 1, 1),
               padding='same',
               kernel_initializer='he_normal')(input_tensor)
    x = BatchNormalization(axis=bn_axis)(x)
    x_after_stage_1 = LeakyReLU(alpha=0.01)(x)

    #x_after_stage_1 = Add()([input_tensor, x])

    # first down convolution
    x_down_conv_1 = projection_block_3D(x_after_stage_1,
                            filters=(32, 32),
                            kernel_size=(2, 2, 2),
                            stage=1,
                            block=1,
                            se_enabled=True,
                            se_ratio=4)

    # second stage
    x = identity_block_3D(x_down_conv_1, filters=(32, 32), kernel_size=(3, 3, 3), stage=2, block=1, se_enabled=True, se_ratio=4)
    x_after_stage_2 = identity_block_3D(x, filters=(32, 32), kernel_size=(3,3,3), stage=2, block=2, se_enabled=True, se_ratio=4)



    # second down convolution
    x_down_conv_2 = projection_block_3D(x_after_stage_2,
                                        filters=(64, 64),
                                        kernel_size=(2, 2, 2),
                                        stage=2,
                                        block=3,
                                        se_enabled=True,
                                        se_ratio=8)

    # third stage
    x = identity_block_3D(x_down_conv_2, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=1, se_enabled=True, se_ratio=8)
    x_after_stage_3 = identity_block_3D(x, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=2, se_enabled=True, se_ratio=8)
    #x = identity_block_3D(x, filters=(64, 64), kernel_size=(3, 3, 3), stage=3, block=3, se_enabled=False, se_ratio=16)

    # third down convolution
    x_down_conv_3 = projection_block_3D(x_after_stage_3,
                                        filters=(128, 128),
                                        kernel_size=(2, 2, 2),
                                        stage=3,
                                        block=4,
                                        se_enabled=True,
                                        se_ratio=16)

    # fourth stage
    x = identity_block_3D(x_down_conv_3, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=1, se_enabled=True, se_ratio=16)
    x_after_stage_4 = identity_block_3D(x, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=2, se_enabled=True, se_ratio=16)
    #x = identity_block_3D(x, filters=(128, 128), kernel_size=(3, 3, 3), stage=4, block=3, se_enabled=False, se_ratio=16)



    ### end of encoder path

    if usingClassification:
        # use x_after_stage_4 as quantification output
        # global average pooling
        x_class = GlobalAveragePooling3D(data_format=K.image_data_format())(x_after_stage_4)

        # fully-connected layer
        classification_output = Dense(units=numClasses,
                       activation='softmax',
                       kernel_initializer='he_normal',
                       name='classification_output')(x_class)


    # create model
    if usingClassification:
        cnn = Model(inputs =[input_tensor], outputs=[classification_output], name='3D-VResFCN-Classification')
        sModelName = cnn.name


    return cnn, sModelName

'''patchSize = (32,32,32)
numClasses = 3

cnn, sModelName = createModel(patchSize, numClasses, usingClassification=False)'''

