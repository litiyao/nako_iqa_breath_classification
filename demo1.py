import os
import pickle
# choose GPU
os.environ["CUDA_VISIBLE_DEVICES"]= "0"

import keras
import keras.backend as K
from keras import callbacks as cb
from keras.models import Sequential, Model
from keras.layers import *
K.set_image_data_format = 'channels_last'

# import own scripts
import utilities
import preprocessing
from training import *
import model_classcification
import histories


# paths to data (patients)
path= "/home/d1259/no_backup/d1259/FCN_breath/"

# path were to save the model and history
path_m = "/home/d1259/no_backup/d1259/FCN_breath/models/"
path_h = "/home/d1259/no_backup/d1259/FCN_breath/histories/"

# list for history-objects
lhist = []

for i in range(1):
    print(' round ' + str(i) + '!')
    print(' load patients and drop last validation patients')

    if i > 0:
        last_val_patients = patients_val # drop last validation from memory
    else:
        last_val_patients = None
    # k=4, 50% out of every 1/4-part, 45 patients discarded (as test-patients)
    patients_test, patients_train, patients_val, patients_val_slices, patients_train_slices = utilities.load_correct_patients(path=path,
                                                                                                  patients_to_take= 9, 	# number of test patients
                                                                                                  forget_slices=True, 	# delete not needed loaded data on RAM
                                                                                                  cut=None, 		    # split for validation- and training-data
                                                                                                  k=3, 			        # k-fold split
                                                                                                  perc=1,		        # sliding window overlap
                                                                                                  iteration=i,		    # k-fold cross-validation run
                                                                                                  last_val_patients=last_val_patients,
                                                                                                  verbose=True)

    '''with open('patients_test.pickle', 'wb') as handle:
        pickle.dump(patients_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('patients_train.pickle', 'wb') as handle:
        pickle.dump(patients_train, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('patients_val.pickle', 'wb') as handle:
        pickle.dump(patients_val, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('patients_val_slices.pickle', 'wb') as handle:
        pickle.dump(patients_val_slices, handle, protocol=pickle.HIGHEST_PROTOCOL)'''

    print(' load model')
    patchSize = (64,64,64)
    numClasses = 3
    cnn, sModelName = model_classcification.createModel(patchSize, numClasses,feed_pos= False)

    # create optimizer
    opti = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    ## opti = keras.optimizers.SGD(lr=0.01)

    cnn.summary()
    # compile model
    cnn.compile(loss=keras.losses.categorical_crossentropy, optimizer=opti, metrics=['accuracy'])

    path_w = path_m + "Fat_dense_64_50_100_no_pos.hdf5"
    checkpointer = cb.ModelCheckpoint(filepath=path_w, verbose=0, monitor='val_loss', save_best_only=True)


    print('Training CNN')


    hist_object = fit_Tiyao(model=cnn,
                      patients_train=patients_train,
                      data_valid=patients_val_slices,
                      data_train = patients_train_slices,
                      epochs= 50,
                      batch_size=48,
                      patient_buffer_capacity=10,  # amount of patients on RAM
                      batches_per_shift= 100,  # batches_per_train_epoch = batches_per_shift * len(patients_train),
                      # batches out of buffer before one shift-operation, see every patient in one epoch!
                      density=5,  # density for meshgrid of positions for validation data
                      border=20,  # distance in pixel between crops
                      callbacks=[checkpointer],  # callback (see keras documentation) for validation loss
                      mult_inputs=False,  # if additional position at bottleneck, mult_inputs = True
                      empty_patient_buffer=True)  # empty whole buffer, after training of one model (provide RAM for next model)

    print('save histories')
    # list of histories
    lhist.append(hist_object.history)

    # save history
    path_hist = path_h + "Fat_dense_64_50_100_no_pos.p"
    histories.save_histories(lhist=lhist, path=path_hist)


'''with open('patients_test.pickle', 'rb') as handle:
    patients_test= pickle.load(handle)

with open('patients_train.pickle', 'rb') as handle:
    patients_train= pickle.load(handle)

with open('patients_val.pickle', 'rb') as handle:
    patients_val= pickle.load(handle)

with open('patients_val_slices.pickle', 'rb') as handle:
    patients_val_slices= pickle.load(handle)'''

'''with open('labels.pickle', 'rb') as handle:
    labels= pickle.load(handle)


with open('labels.pickle', 'wb') as handle:
    pickle.dump(labels, handle, protocol=pickle.HIGHEST_PROTOCOL)'''

